import 'package:flutter/material.dart';
import 'package:open_color/Modulos/Menu/Menu_list_view.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter/services.dart';

Future main() async {
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}
class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 4,
        navigateAfterSeconds: new MyAppAfterSplash(),
        image: new Image.asset("assets/Imagenes/open2.png"),
        backgroundColor: Colors.black,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 100.0,
        //onClick: ()=>print("Flutter Egypt"),
        loaderColor: Colors.white
    );
  }
}

class MyAppAfterSplash extends StatelessWidget {
  final appTitle = 'iChillán';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MenusPage(),
    );
  }
}
