import 'package:flutter/material.dart';
import 'package:open_color/Modulos/AgregarUsuarios/Usuarios.dart';
import 'package:open_color/Modulos/Inicio/Configuracion.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class InicioPageState extends State<InicioPage> {
  List<Configuracion> configurationList = [];

  @override
  void initState() {
    super.initState();

    CollectionReference configurationRef =
        Firestore.instance.collection("Configuracion");

    configurationRef.snapshots().listen((querySnapshot) {
      querySnapshot.documentChanges.forEach((document) {
        if (document.document['activado'] == "Verdadero") {
          Configuracion configuration = new Configuracion(
              document.document['activado'],
              document.document['texto'],
              document.document['url'],
              document.document['color1'],
              document.document['color2'],
              document.document['color3']);
          //print(document.document['url']);
          configurationList.add(configuration);
        }
      });
      setState(() {
        print('Length: ' + configurationList.length.toString());
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    Widget w_eventos;
    Widget w_portada = Image.asset("assets/Imagenes/portada.jpg", height: mediaQueryData.height*0.5,);
    Widget w_inicio = Text("¿Te lo vas a perder?",
        textAlign: TextAlign.center,
        style:
            TextStyle(color: Colors.white, fontFamily: 'Avenir', fontSize: 25));

    w_eventos = StreamBuilder(
        stream: Firestore.instance.collection("Fotos").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container();
          return Card(
              child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            child: Container(
              //height: mediaQueryData.height*0.30,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                    child: Text('Próximos eventos',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Avenir',
                            fontSize: 15)),
                  ),
                  CarouselSlider(
                    //height: 400.0,
                    autoPlayInterval: Duration(seconds: 5),
                    autoPlayAnimationDuration: Duration(milliseconds: 800),

                    autoPlay: true,
                    items: snapshot.data.documents
                        .map<Widget>((DocumentSnapshot document) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: 2,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            //decoration: BoxDecoration(
                            //    color: Colors.amber
                            //),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                              child: CachedNetworkImage(
                                imageUrl: document['url'],
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          );
                        },
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          ));
        });

    return Scaffold(
        body: //Column(
            //children: <Widget>[
            Container(
      height: mediaQueryData.height,
      decoration: new BoxDecoration(color: Color.fromRGBO(19, 19, 19, 1)),
      child: (SingleChildScrollView(
          child: Column(
        children: <Widget>[
          Center(
            child: w_portada,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
            child: Center(
              child: w_inicio,
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginPage()),
              );
            },
            child: configurationUI(
                "Falso", "QUIERO ACCEDER A DESCUENTOS", "", "236", "66", "139"),
          ),
          configurationList.length == 0
              ? new Container()
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: configurationList.length,
                  itemBuilder: (_, index) => configurationUI(
                      configurationList[index].activado,
                      configurationList[index].texto,
                      configurationList[index].url,
                      configurationList[index].color1,
                      configurationList[index].color2,
                      configurationList[index].color3))
        ],
      ))),
    ));
  }

  Widget configurationUI(String activado, String texto, String url,
      String color1, String color2, String color3) {
    final mediaQueryData = MediaQuery.of(context).size;
    Widget retorno;
    (activado == "Verdadero")
        ? retorno = GestureDetector(
            onTap: () async {
              if (await canLaunch(url)) {
                await launch(url);
              }
            },
            child: Card(
                child: InkWell(
                    splashColor: Colors.blue.withAlpha(30),
                    child: Container(
                      width: mediaQueryData.width,
                      height: 60,
                      decoration: new BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(3)),
                          shape: BoxShape.rectangle,
                          //border: Border.all(width: 3,color: Colors.green,style: BorderStyle.solid),
                          color: Color.fromRGBO(int.parse(color1),
                              int.parse(color2), int.parse(color3), 1)),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Stack(
                          children: <Widget>[
                            Center(
                              child: Text(texto,
                                  //textAlign: TextAlign.left,
                                  style: TextStyle(
                                    fontFamily: 'Avenir',
                                    color: Colors.white,
                                  )),
                            ),
                            Container(
                              alignment: Alignment.centerRight,
                              child: Image.asset(
                                "assets/Imagenes/flecha-derecha.png",
                                width: 30,
                              ),
                            )
                          ],
                        ),
                      ),
                    ))),
          )
        : retorno = Card(
            child: InkWell(
                splashColor: Colors.blue.withAlpha(30),
                child: Container(
                  width: mediaQueryData.width,
                  height: 60,
                  decoration: new BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(3)),
                      shape: BoxShape.rectangle,
                      //border: Border.all(width: 3,color: Colors.green,style: BorderStyle.solid),
                      color: Color.fromRGBO(int.parse(color1),
                          int.parse(color2), int.parse(color3), 1)),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Stack(
                      children: <Widget>[
                        Center(
                          child: Text(texto,
                              //textAlign: TextAlign.left,
                              style: TextStyle(
                                fontFamily: 'Avenir',
                                color: Colors.white,
                              )),
                        ),
                        Container(
                          alignment: Alignment.centerRight,
                          child: Image.asset(
                            "assets/Imagenes/flecha-derecha.png",
                            width: 30,
                          ),
                        )
                      ],
                    ),
                  ),
                )));

    return retorno;
  }
}

class InicioPage extends StatefulWidget {
  @override
  createState() => InicioPageState();
}
