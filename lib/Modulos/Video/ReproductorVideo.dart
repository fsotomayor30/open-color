import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_youtube/flutter_youtube.dart';
import 'package:flutter/cupertino.dart';

import 'VideoModel.dart';

class Video1 extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<Video1> {
  TextEditingController textEditingControllerUrl = new TextEditingController();
  TextEditingController textEditingControllerId = new TextEditingController();
  List<VideoModel> videosList = [];

  @override
  initState() {
    super.initState();

    CollectionReference configurationRef =
        Firestore.instance.collection("Video");

    configurationRef.snapshots().listen((querySnapshot) {
      querySnapshot.documentChanges.forEach((document) {
        VideoModel video = new VideoModel(
            document.document['url'], document.document['nombre']);
        //print(document.document['url']);
        videosList.add(video);
      });
      setState(() {
        print('Length: ' + videosList.length.toString());
      });
    });
  }

  void playYoutubeVideo() {
    FlutterYoutube.playYoutubeVideoByUrl(
      apiKey: "AIzaSyCwgh2sHtkTN4n2WSVCTu_Q6KvXOKjY3Ig",
      videoUrl: "https://www.youtube.com/watch?v=wgTBLj7rMPM",
    );
  }

  void playYoutubeVideoEdit(String url) {
    FlutterYoutube.onVideoEnded.listen((onData) {
      //perform your action when video playing is done
    });

    FlutterYoutube.playYoutubeVideoByUrl(
      apiKey: "AIzaSyCwgh2sHtkTN4n2WSVCTu_Q6KvXOKjY3Ig",
      videoUrl: url,
    );
  }

  void playYoutubeVideoIdEdit() {
    FlutterYoutube.onVideoEnded.listen((onData) {
      //perform your action when video playing is done
    });

    FlutterYoutube.playYoutubeVideoById(
      apiKey: "AIzaSyCwgh2sHtkTN4n2WSVCTu_Q6KvXOKjY3Ig",
      videoId: textEditingControllerId.text,
    );
  }

  void playYoutubeVideoIdEditAuto() {
    FlutterYoutube.onVideoEnded.listen((onData) {
      //perform your action when video playing is done
    });

    FlutterYoutube.playYoutubeVideoById(
        apiKey: "AIzaSyCwgh2sHtkTN4n2WSVCTu_Q6KvXOKjY3Ig",
        videoId: textEditingControllerId.text,
        autoPlay: true);
  }

/*  @override
  Widget build(BuildContext context) {
    return Column(
            children: <Widget>[
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextField(
                  controller: textEditingControllerUrl,
                  decoration:
                  new InputDecoration(labelText: "Enter Youtube URL"),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new RaisedButton(
                    child: new Text("Play Video By Url"),
                    onPressed: playYoutubeVideoEdit),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new RaisedButton(
                    child: new Text("Play Default Video"),
                    onPressed: playYoutubeVideo),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new TextField(
                  controller: textEditingControllerId,
                  decoration: new InputDecoration(
                      labelText: "Youtube Video Id (wgTBLj7rMPM)"),
                ),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new RaisedButton(
                    child: new Text("Play Video By Id"),
                    onPressed: playYoutubeVideoIdEdit),
              ),
              new Padding(
                padding: const EdgeInsets.all(10.0),
                child: new RaisedButton(
                    child: new Text("Auto Play Video By Id"),
                    onPressed: playYoutubeVideoIdEditAuto),
              ),
            ],
          );
  }*/

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return Container(
      height: mediaQueryData.height,
      decoration: new BoxDecoration(color: Color.fromRGBO(19, 19, 19, 1)),
      child: (SingleChildScrollView(
          child:
          videosList.length == 0
              ? new Container()
              : ListView.builder(
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemCount: videosList.length,
                  itemBuilder: (_, index) =>
                      videoUI(videosList[index].url, videosList[index].nombre))

      )),
    );
  }

  Widget videoUI(String url, String nombre) {
    final mediaQueryData = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: () async {
        playYoutubeVideoEdit(url);
      },
      child: Card(
          child: InkWell(
              splashColor: Colors.blue.withAlpha(30),
              child: Container(
                //width: mediaQueryData.width - 20,
                //height: 60,
                decoration:
                    new BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(3)),
                        shape: BoxShape.rectangle,
                        color: Color.fromRGBO(237, 56, 51, 1)),
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        width: mediaQueryData.width-70,
                        child: Center(
                          child: Text(nombre,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontFamily: 'Avenir',
                                color: Colors.white,
                              )),
                        ),
                      ),
                      Container(
                        alignment: Alignment.centerRight,
                        child: Image.asset(
                          "assets/Imagenes/play.png",
                          width: 30,
                        ),
                      )
                    ],
                  ),
                ),
              ))),
    );
  }
}
