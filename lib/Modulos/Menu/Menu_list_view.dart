import 'package:flutter/material.dart';
import 'package:open_color/Modulos/Auspiciadores/Auspiciadores.dart';
import 'package:open_color/Modulos/Galeria/Fotos.dart';
import 'package:open_color/Modulos/Video/VideoPage.dart';


import 'package:open_color/Modulos/Inicio/Inicio.dart';

import 'package:url_launcher/url_launcher.dart';


class MenusPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MenuList(),
    );
  }
}

class MenuList extends StatelessWidget{


  List<Widget> containers = [
    Container(
      child: InicioPage()
    ),
    Container(
      child: FotosPage(),
    ),
    Container(
      child: VideoPage(),
    ),

    Container(
      child: Auspiciadores(),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(19,19,19,1),

          title: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Image.asset('assets/Imagenes/icon.png'),
              ),
             // Text('iChillán'),
            ],
          ),
          bottom : TabBar(indicatorColor: Colors.white,tabs: <Widget>[
            Tab(
                icon: Icon(Icons.home),
            ),
            Tab(
              icon: Icon(Icons.image)
            ),
            Tab(
                icon: Icon(Icons.play_circle_filled)
            ),
            Tab(
              icon: Icon(Icons.group)
            )
          ]),
        ),
        body: TabBarView(children: containers),
        drawer: Drawer(
          // Add a ListView to the drawer. This ensures the user can scroll
          // through the options in the drawer if there isn't enough vertical
          // space to fit everything.
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(19,19,19,1),
            ),
          child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                child: Image.asset('assets/Imagenes/open2.png'),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(19,19,19,1),
                ),
              ),
              ListTile(
                title: Text('Facebook', style: TextStyle(fontFamily: 'Avenir',
                    color: Colors.white),),
                leading: CircleAvatar(
                  child: Image.asset('assets/Imagenes/facebook.png'),
                  backgroundColor: Colors.transparent,
                ),
                onTap: () async {
                  if (await canLaunch("https://www.facebook.com/opencolorstour/")) {
                    await launch("https://www.facebook.com/opencolorstour/");
                  }
                },
                trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white,),

              ),
              ListTile(
                title: Text('Instagram', style: TextStyle(fontFamily: 'Avenir',
                    color: Colors.white),),
                leading: CircleAvatar(
                  child: Image.asset('assets/Imagenes/instagram.png'),
                  backgroundColor: Colors.transparent,
                ),
                trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white,),
                onTap: () async {
                  if (await canLaunch("https://www.instagram.com/opencolors.tour/?hl=es-la")) {
                    await launch("https://www.instagram.com/opencolors.tour/?hl=es-la");
                  }
                },
              ),


            ],
          ),
        ),
        ),
      ),
    );
  }


}
