import 'package:flutter/material.dart';
import 'package:open_color/Modulos/Menu/Menu_list_view.dart';
import 'package:validate/validate.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new _LoginPageState();
}

class _LoginData {
  String email = '';
  String telefono = '';
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  _LoginData _data = new _LoginData();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  String _validateEmail(String value) {
    // If empty value, the isEmail function throw a error.
    // So I changed this function with try and catch.
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'Ingresa un correo electrónico correcto.';
    }

    return null;
  }

  String _validateTelefono(String value) {
    if (value.length < 12) {
      return 'Ingresa un teléfono correcto.';
    }

    return null;
  }

  void submit() {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.

      print('Printing the login data.');
      print('Email: ${_data.email}');
      print('Telefono: ${_data.telefono}');
      createRecord(_data.email, _data.telefono);
      _scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text("Tu correo sólo será ocupado para realizar los descuentos al momento de tu compra"),
            duration: Duration(seconds: 10),
            action: SnackBarAction(
              label: 'OK  ',
              onPressed: (){
                _scaffoldKey.currentState.hideCurrentSnackBar();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MenusPage()),
                );
                },
            ),
          )
      );

  }
  }


  createRecord(String email, String telefono) async {
    var now = new DateTime.now();
    DocumentReference ref = await Firestore.instance
        .collection("Usuarios")
        .add({
      'correo': email,
      'telefono': telefono,
      'fecha': now
    });

    print(ref.documentID);

  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: Color.fromRGBO(19,19,19,1),
        title: new Text('Registro de fans'),
      ),
      body: new Container(
          padding: new EdgeInsets.all(20.0),
          child: new Form(
            key: this._formKey,
            child: new ListView(
              children: <Widget>[
                new TextFormField(
                    keyboardType: TextInputType.emailAddress, // Use email input type for emails.
                    decoration: new InputDecoration(
                        hintText: 'open@colors.cl',
                        labelText: 'Correo Electrónico'
                    ),
                    validator: this._validateEmail,
                    onSaved: (String value) {
                      this._data.email = value;
                    }
                ),
                new TextFormField(
                    keyboardType: TextInputType.phone,
                    decoration: new InputDecoration(
                        hintText: '+569 12345678',
                        labelText: 'Ingresa tu teléfono'
                    ),
                    validator: this._validateTelefono,
                    onSaved: (String value) {
                      this._data.telefono = value;
                    }
                ),
                new Container(
                  width: screenSize.width,
                  child: new RaisedButton(

                    child: new Text(
                      'Registrarme',
                      style: new TextStyle(
                          color: Colors.white
                      ),
                    ),
                    onPressed: this.submit,
                    color: Colors.black,
                  ),
                  margin: new EdgeInsets.only(
                      top: 20.0
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Image.asset("assets/Imagenes/icon.png"),
                )
              ],
            ),
          )
      ),
    );
  }
}