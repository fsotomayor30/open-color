import 'package:flutter/material.dart';
import 'package:open_color/Modulos/Video/ReproductorVideo.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class FotosPageState extends State<FotosPage> {


  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;
    Widget w_fotos;
    Widget w_djs;


    w_fotos=
        StreamBuilder(
            stream: Firestore.instance.collection("Fotos").snapshots(),
            builder: (context, snapshot){
              if(!snapshot.hasData) return Container();
              return
                Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      child: Container(
                        decoration: new BoxDecoration(
                            color: Color.fromRGBO(19,19,19,1)
                        ),
                        //width: mediaQueryData.width*0.9,
                        //height: mediaQueryData.height*0.30,
                        child:Column(
                          children: <Widget>[
                            CarouselSlider(
                              //height: mediaQueryData.height*0.5,
                              autoPlayInterval: Duration(seconds: 5),
                              autoPlayAnimationDuration: Duration(milliseconds: 800),

                              autoPlay: true,
                              items: snapshot.data.documents.map<Widget>((DocumentSnapshot document) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 2,
                                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                                      //decoration: BoxDecoration(
                                      //    color: Colors.amber
                                      //),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: CachedNetworkImage(
                                          imageUrl:  document['url'],
                                          placeholder: (context, url) => CircularProgressIndicator(),
                                          errorWidget: (context, url, error) => Icon(Icons.error),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                    ));
            });
    w_djs=
        StreamBuilder(
            stream: Firestore.instance.collection("Djs").snapshots(),
            builder: (context, snapshot){
              if(!snapshot.hasData) return Container();
              return
                Card(
                    child: InkWell(
                      splashColor: Colors.blue.withAlpha(30),
                      child: Container(
                        decoration: new BoxDecoration(
                            color: Color.fromRGBO(19,19,19,1)
                        ),
                        //width: mediaQueryData.width*0.9,
                        //height: mediaQueryData.height*0.30,
                        child:Column(
                          children: <Widget>[
                            CarouselSlider(
                              //height: mediaQueryData.height*0.5,
                              autoPlayInterval: Duration(seconds: 5),
                              autoPlayAnimationDuration: Duration(milliseconds: 800),

                              autoPlay: true,
                              items: snapshot.data.documents.map<Widget>((DocumentSnapshot document) {
                                return Builder(
                                  builder: (BuildContext context) {
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 2,
                                      margin: EdgeInsets.symmetric(horizontal: 5.0),
                                      //decoration: BoxDecoration(
                                      //    color: Colors.amber
                                      //),
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                                        child: CachedNetworkImage(
                                          imageUrl:  document['url'],
                                          placeholder: (context, url) => CircularProgressIndicator(),
                                          errorWidget: (context, url, error) => Icon(Icons.error),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }).toList(),
                            ),
                          ],
                        ),
                      ),
                    ));
            });



    return Scaffold(
        body: //Column(
        //children: <Widget>[
        Container(
          height: mediaQueryData.height,
          decoration: new BoxDecoration(
              color: Color.fromRGBO(19,19,19,1)
          ),
          child: (SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Text("Nuestros eventos",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Avenir',
                      color: Colors.white,
                      fontSize: 25
                  )
              ),
                  Center(
                    child: w_fotos,
                  ),
              Text("Line Up",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontFamily: 'Avenir',
                      color: Colors.white,
                      fontSize: 25
                  )
              ),
                  Center(
                    child: w_djs
                  )

                ],
              ))


              //],
              //)
          ),
        )
    );
  }
}

class FotosPage extends StatefulWidget {
  @override
  createState() => FotosPageState();
}
